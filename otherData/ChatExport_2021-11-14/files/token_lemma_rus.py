pip install pymorphy2
pip install textcleaner

# импортирование библиотек для проекта
import re
import pandas as pd
import numpy as np
from datetime import datetime

from textblob import TextBlob
import pymorphy2
import collections, re
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator
import nltk
nltk.download("stopwords")
from nltk.corpus import stopwords
from string import punctuation

# загрузка исходных данных
query_rank=pd.read_csv('/content/drive/MyDrive/WB_hackathon/query_popularity.csv',header=0)
query_rank.columns=['query','rank']

# отображение первых строк исходного файла
query_rank.dropna(inplace=True)
query_rank=query_rank.reset_index(drop=True)

import textcleaner as tc
from nltk.corpus import stopwords

def token_lemma_russ (text):
  tokens=re.findall(r'[а-яА-Я]+', str(text).lower())

  morph =pymorphy2.MorphAnalyzer()
  txt=''
  for token in tokens:
    if token not in stopwords.words('russian'):
      txt+=morph.parse(token)[0].normal_form+' '
  return txt

# подсчет количества слов в каждом популярном запросе
query_rank['cnt_words']=''
query_rank['words']=''
for i in range (len(query_rank)):
  text=query_rank['query'][i]
  query_rank['words'][i]=token_lemma_russ (text) 
  query_rank['cnt_words'][i]=len(str(query_rank['words'][i]).split())

  
