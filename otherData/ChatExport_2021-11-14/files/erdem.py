#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import pandas as pd
import textblob
from textblob import TextBlob
import pymorphy2
import collections, re
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator
import nltk
nltk.download("stopwords")
from nltk.corpus import stopwords
from string import punctuation

df = pd.read_csv(r'C:\Users\e.tsybenov\Documents\Machine Learning\Other\search_history.csv', header=0)
df.info()

df.head(30)

df = df.astype({"time": 'datetime64[ns]'})

df.isnull().sum().sort_values(ascending=False).head()

df['wbuser_id'].value_counts().head(12)

# пользователь с 18 000К запросов
df[df['wbuser_id']=='70311ec9008a31f743c164e6f1198c86'].sort_values(by=['weekday','time','UQ']).head(12)

unauth_user_stat = df[df['wbuser_id']=='70311ec9008a31f743c164e6f1198c86']['locale'].value_counts()
unauth_user_stat

unauth_user_stat.groupby(['UQ']).count().sort_values(by=['time'],ascending=False).head(12)

# пользователь с 20К запросов
second_user = df[df['wbuser_id']=='e33b6cbd5a3983f2b895dd8eaa8ec5cb'].sort_values(by=['weekday','time'])
second_user

second_user.groupby(['UQ']).count().sort_values(by=['time'],ascending=False).head(12)
len(second_user['UQ'].unique())

# мешок слов для locale = Kg
bags_id=[]
data=df[df['locale']==Kg]
ids=data['wbuser_id'].unique()
for wbuser_id in ids:
    data_id=data[data['wbuser_id']==wbuser_id]
    bagsofwords = collections.Counter([y for x in data_id['UQ'].values.flatten() for y in re.findall(r'[а-яА-Я]+', str(x).lower())])
    bags_id.append([wbuser_id,bagsofwords])

