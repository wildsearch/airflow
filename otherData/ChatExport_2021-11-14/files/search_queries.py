import pyowm
owm=pyowm.OWM('816e642439aa601255be01c741e825bf')

# рекомендация по сезону и погоде
def season (country):
  owm=pyowm.OWM('816e642439aa601255be01c741e825bf')
  
  mount_now=datetime.now().month
  season=['зима','весна','лето','осень']
  
  mgr = owm.weather_manager()
  observation=mgr.weather_at_place(country)
  weather = observation.weather
  temperature=weather.temperature('celsius')['temp']
  #status=weather.status    # может указывать на осадки

  temp_mean=[[-10,-5],[-10,-3],[-5,2],[2,11],[8,19],[12,22],[14,24],[12,22],[7,16],[2,8],[-3,1],[-7,-3]]
  if temp_mean[mount_now][0]>=temperature:                                            
    dop_season=season[(mount_now//3+1)%4] # предлагаем следующую сезоность
  elif temperature >= temp_mean[mount_now][1]:
    dop_season='демисезонность'
  else:
    dop_season=season[mount_now//3]

  if mount_now==12:
    mount_now-=12
  return season[mount_now//3], dop_season

# рекомендация по рейтингу продаж (допиливаю)
def sales(query):
  rank_sales={'куртка':{'твидовый':12,'пуховик':1,'синтепоновая':10},
              'ботинки':{'кожанные':1,'reima':15,'прорезиненная':8}}
  sale_1=sorted(rank_sales[query])[-1]
  sale_2=sorted(rank_sales[query])[-2]
  return sale_1,sale_2

# префиксы
perfixes=['полу']


# регистрация пользователя (история покупок)
registration={'gender': 'женский',
         'city':'Moscow'}


# запрос клиента (данные из поисковой строки)
#query является ключом для словарей
query=input('Поисковая строка: ')



# выдача рекомендованного запроса по шаблону 
#[история клиента] [запрос на сайте][сезонная рекомендация][рекомендация по продажам]

print('Поисковые рекомендации')
print('______________________')
print('{} {} {} {}'.format(registration['gender'], query,season (registration['city'])[0],sales(query)[1]))
print('{} {} {} {}'.format(registration['gender'], perfixes[0]+query,season (registration['city'])[1],sales(query)[0]))
