import pandas as pd
import matplotlib.pyplot as plt
import src.wrangling_dataset as wd

def main():
    data_path = '/app/search_history_tab.csv'

    search_tp = pd.read_csv(data_path, sep='\t', iterator=True, chunksize=10000)
    search_df = pd.concat(search_tp, ignore_index=True)
    search_df.head()

    wd.verify_data_quality(search_df)

    search_df.describe()

    search_df['wbuser_id'].value_counts().head(20)

    search_df['UQ'].value_counts().head(20)

    search_df['cnt'].value_counts().head(20)

    search_df['locale'].value_counts().head(20)

    search_df['weekday'].value_counts().head(20)

    search_df['time'] = pd.to_datetime(search_df['time']).dt.time
    search_df = search_df.set_index(['time'])

    search_df.head()

    # Delete NA
    search_df = search_df.dropna()

    # Удаление дублирующих значений
    search_df = search_df.drop_duplicates()

    search_df['wbuser_id'].value_counts().head(20)

if __name__ == "__main__":
    main()