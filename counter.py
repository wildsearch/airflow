import pandas as pd
import matplotlib.pyplot as plt
import src.lemma as lemma

def main():
    # загрузка исходных данных
    query_rank=pd.read_csv('/app/query_popularity.csv',header=0)
    query_rank.columns=['query','rank']

    # отображение первых строк исходного файла
    query_rank.dropna(inplace=True)
    query_rank=query_rank.reset_index(drop=True)

    # подсчет количества слов в каждом популярном запросе
    query_rank['cnt_words']=''
    query_rank['words']=''
    for i in range (len(query_rank)):
        text=query_rank['query'][i]
        query_rank['words'][i]=lemma.token_lemma_russ(text) 
        query_rank['cnt_words'][i]=len(str(query_rank['words'][i]).split())

if __name__ == "__main__":
    main()