# импортирование библиотек для проекта
import re
import pandas as pd
import numpy as np
from datetime import datetime

from textblob import TextBlob
import pymorphy2
import collections, re
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator
import nltk
nltk.data.path.append('/app/logs/nltk_cache')
nltk.download("stopwords")
from nltk.corpus import stopwords
from string import punctuation

import textcleaner as tc
from nltk.corpus import stopwords

def token_lemma_russ (text):
  tokens=re.findall(r'[а-яА-Я]+', str(text).lower())

  morph =pymorphy2.MorphAnalyzer()
  txt=''
  for token in tokens:
    if token not in stopwords.words('russian'):
      txt+=morph.parse(token)[0].normal_form+' '
  return txt



  
