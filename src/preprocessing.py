import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
plt.style.use('bmh')
import seaborn as sns
sns.set_style('whitegrid')

import tensorflow as tf

import nltk
nltk.data.path.append('/app/logs/nltk_cache')
from nltk import word_tokenize
from nltk.tokenize import word_tokenize,sent_tokenize
from nltk.corpus import stopwords
from nltk.tokenize import RegexpTokenizer
from nltk.stem import WordNetLemmatizer
from nltk import word_tokenize,pos_tag
from nltk.stem import PorterStemmer
import re

from pymystem3 import Mystem
from string import punctuation

from spacy.lang.ru import Russian
from nltk.stem.snowball import SnowballStemmer





# Lowercasing
def lowercasing(query):
    return  query.lower()

# Remove Extra Whitespaces

def remove_whitespace(query):
    return  " ".join(query.split())

# Character Tokenization
def character_tokenization(query):
    character = [x for x in query]
    return  character

# Word Tokenization
def word_tokenization(query):
    query = word_tokenize(query)
    return  query

# Sentence Tokenization
def sentence_tokenization(query):
    query = sent_tokenize(query)
    return  query

# Whitespace Tokenization
def whitespace_tokenization(query):
    from nltk.tokenize import WhitespaceTokenizer
    
    Tokenizer=WhitespaceTokenizer()
    query = Tokenizer.tokenize(query)
    return  query


# Removing Stopword
def remove_stopwords(word_list):
        processed_word_list = []
        for word in word_list:
            word = word.lower() # in case they arenet all lower cased
            if word not in stopwords.words("russian"):
                processed_word_list.append(word)
        return processed_word_list
    
# Removing Punctuations
def remove_punct(query):
    tokenizer = RegexpTokenizer(r"\w+")
    lst=tokenizer.tokenize(' '.join(query))
    return lst




# Removal of Tags
def remove_tag(query):
    
    text=' '.join(query)
    html_pattern = re.compile('<.*?>')
    return html_pattern.sub(r'', text)

# Text prepocessing with Word Tokenization
def preprocess_WT(query):
    
    #Step_1 Lowercasing
    query = lowercasing(query)
    
    #Step_2 Removing Extra Whitespaces
    query = remove_whitespace(query)
    
    #Step_3 Tokenization
    query = word_tokenization(query)
    
    #Step_4 Stopwords Removal
    query = remove_stopwords(query)
    
    #Step_5 Removing Punctuations
    query = remove_punct(query)
    
    #Step_6 Removal of Tags
    query = remove_tag(query)
    return query

# Text prepocessing with Sentence Tokenization
def preprocess_ST(query):
    
    #Step_1 Lowercasing
    query = lowercasing(query)
    
    #Step_2 Removing Extra Whitespaces
    query = remove_whitespace(query)
    
    #Step_3 Tokenization
    query = sentence_tokenization(query)
    
    #Step_4 Stopwords Removal
    query = remove_stopwords(query)
    
    #Step_5 Removing Punctuations
    query = remove_punct(query)
    
    #Step_6 Removal of Tags
    query = remove_tag(query)
    return query

# Text prepocessing with Character Tokenization
def preprocess_CT(query):
    
    #Step_1 Lowercasing
    query = lowercasing(query)
    
    #Step_2 Removing Extra Whitespaces
    query = remove_whitespace(query)
    
    #Step_3 Tokenization
    query = character_tokenization(query)
        
    #Step_5 Removing Punctuations
    query = remove_punct(query)
    
    #Step_6 Removal of Tags
    query = remove_tag(query)
    return query

# Text prepocessing with Whitespace Tokenization
def preprocess_WST(query):
    
    #Step_1 Lowercasing
    query = lowercasing(query)
    
    #Step_2 Removing Extra Whitespaces
    query = remove_whitespace(query)
    
    #Step_3 Tokenization
    query = whitespace_tokenization(query)
    
    #Step_4 Stopwords Removal
    query = remove_stopwords(query)
    
    #Step_5 Removing Punctuations
    query = remove_punct(query)
    
    #Step_6 Removal of Tags
    query = remove_tag(query)
    return query

# Lemmatization
def lemmatization(text):
    nlp = Russian()
    doc = nlp(text)
    tokens = [token.lemma_ for token in doc]
    return " ".join(tokens)

# Stemming
def stemming(query):
    stemmer = SnowballStemmer(language='russian')
    
    result=[]
    for word in query:
        result.append(stemmer.stem(word))
    return result