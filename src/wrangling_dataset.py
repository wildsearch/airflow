import matplotlib.pyplot as plt
plt.style.use('bmh')
import seaborn as sns
sns.set_style('whitegrid')


def verify_data_quality(evaluated_dataset):
    '''
    ФУНКЦИЯ КОМПЛЕКСНОЙ ОЦЕНКИ КАЧЕСТВА ДАННЫХ
    '''
    # Проверка типов данных
    print("Типы данных:\n{}\n".format(evaluated_dataset.dtypes) + '_' * 50+ '\n')
   
    # Проверка наличия значений NaN
    print("Общее количество пропущенных значений:\n{}\n".format(evaluated_dataset.isnull().sum().sum()) + '_' * 50+ '\n')
    
    evaluated_dataset.isnull().sum().plot(kind='bar')
    # Add a title and show the plot.
    plt.title('Распределение пропущенных значений по столбцам')
    # Create tick mark labels on the Y axis and rotate them.
    plt.xticks(rotation = 90)
    # Create X axis label.
    plt.xlabel("Columns")
    # Create Y axis label.
    plt.ylabel("NaN Values");
    
    # Количество значений, отличных от NaN
    print("Количество значений, отличных от NaN:\n{}\n".format(evaluated_dataset.count().sum()) + '_' * 50+ '\n')
    
    # Поиск дублирующих строк 
    print("Дублирующих строк:\n{}\n".format(evaluated_dataset.duplicated().sum()) + '_' * 50+ '\n')
    
    # Форма нарбора данных
    print("Форма нарбора данных:\n{}\n".format(evaluated_dataset.shape) + '_' * 50+ '\n')
   
    # Тип индекса набора данных
    print("Тип индекса набора данных:\n{}\n".format(evaluated_dataset.index) + '_' * 50+ '\n')