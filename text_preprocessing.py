import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
plt.style.use('bmh')
import seaborn as sns
sns.set_style('whitegrid')

import tensorflow as tf

import nltk
nltk.data.path.append('/app/logs/nltk_cache')
from nltk import word_tokenize
from nltk.tokenize import word_tokenize,sent_tokenize
from nltk.corpus import stopwords
from nltk.tokenize import RegexpTokenizer
from nltk.stem import WordNetLemmatizer
from nltk import word_tokenize,pos_tag
from nltk.stem import PorterStemmer
import re

from pymystem3 import Mystem
from string import punctuation

from spacy.lang.ru import Russian
from nltk.stem.snowball import SnowballStemmer

import src.preprocessing as pp

"""## Load Raw text

**Популярность запросов** — относительная популярность запросов за тот же период, что и в файле с Историей Поиска.

- query - текст запроса

- query_popularity - относительная популярность запроса за взятый период. 1 наименее популярный, 10 — наиболее
"""
def main():

    query_df = pd.read_csv('/app/query_popularity.csv')
    query_df.head()

    test_query_df = query_df.loc[:10]
    test_query_df

    """**История поиска** — реальные данные взятые за определенный период (неделя). 

    - wbuser_id - обезличенный идентификатор пользователя

    - UQ - введенный поисковый запрос

    - cnt - количество товаров в выдаче, не считая дополнительных рекомендаций

    - locale - локаль сайта, на которой был введен запрос 

    - weekday - день недели запроса

    - time - время запроса
    """

    # search_df = pd.read_csv('search_history.csv',  names= ['wbuser_id','UQ','cnt','locale','weekday','time'],  low_memory=True)
    # search_df.head()

    """## EDA"""

    # Check the missing values
    print("Check the number of records")
    print("Number of records: ", query_df.shape[0], "\n")

    print("Null analysis")
    empty_sample = query_df[query_df.isnull().any(axis=1)]
    print("Number of records contain 1+ null: ", empty_sample.shape[0], "\n")

    # # Check the missing values
    # print("Check the number of records")
    # print("Number of records: ", search_df.shape[0], "\n")

    # print("Null analysis")
    # empty_sample = search_df[search_df.isnull().any(axis=1)]
    # print("Number of records contain 1+ null: ", empty_sample.shape[0], "\n")

    # Drop NA
    query_df = query_df.dropna()
    # search_df = search_df.dropna()

    # Check the missing values
    print("Check the number of records")
    print("Number of records: ", query_df.shape[0], "\n")

    print("Null analysis")
    empty_sample = query_df[query_df.isnull().any(axis=1)]
    print("Number of records contain 1+ null: ", empty_sample.shape[0], "\n")

    # # Check the missing values
    # print("Check the number of records")
    # print("Number of records: ", search_df.shape[0], "\n")

    # print("Null analysis")
    # empty_sample = search_df[search_df.isnull().any(axis=1)]
    # print("Number of records contain 1+ null: ", empty_sample.shape[0], "\n")

    """### Preprocessing function """

    nltk.download('punkt')
    nltk.download('stopwords')
    nltk.download('averaged_perceptron_tagger')

    test_query_df['query'].apply(pp.preprocess_WT)

    test_query_df['query'].apply(pp.preprocess_ST)

    test_query_df['query'].apply(pp.preprocess_CT)

    test_query_df['query'].apply(pp.preprocess_WST)

    test_query_df['query'] = test_query_df['query'].apply(pp.preprocess_WT)
    test_query_df['query']

    pp.lemmatization(test_query_df['query'].to_string())

    text = 'куртка куртки курток' 

    pp.lemmatization(text)

    # Stemming preprocess_WT
    test_query_df['query'] = test_query_df['query'].apply(pp.stemming)
    query_df.head()

    text = ['куртка', 'куртки', 'курток'] 

    pp.stemming(text)

if __name__ == "__main__":
    main()